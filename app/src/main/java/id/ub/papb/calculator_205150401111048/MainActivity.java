package id.ub.papb.calculator_205150401111048;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText;
    Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9;
    Button btTambah, btKurang, btKali, btBagi;
    Button btEnter, btClear;

    public static double nilai = 0;
    public static double hasil = 0.0;
    public static String operasi = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    void init(){
        editText = (EditText) findViewById(R.id.editText);
        bt0 = (Button)findViewById(R.id.bt0);
        bt0.setOnClickListener(this);
        bt1 = (Button)findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button)findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button)findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button)findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button)findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button)findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button)findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button)findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button)findViewById(R.id.bt9);
        bt9.setOnClickListener(this);

        btTambah = (Button)findViewById(R.id.btTambah);
        btTambah.setOnClickListener(this);
        btKurang = (Button)findViewById(R.id.btKurang);
        btKurang.setOnClickListener(this);
        btKali = (Button)findViewById(R.id.btKali);
        btKali.setOnClickListener(this);
        btBagi = (Button)findViewById(R.id.btBagi);
        btBagi.setOnClickListener(this);

        btEnter = (Button)findViewById(R.id.btEnter);
        btEnter.setOnClickListener(this);
        btClear = (Button)findViewById(R.id.btClear);
        btClear.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt0:
                editText.setText(editText.getText().toString().trim()+"0");
                break;
            case R.id.bt1:
                editText.setText(editText.getText().toString().trim()+"1");
                break;
            case R.id.bt2:
                editText.setText(editText.getText().toString().trim()+"2");
                break;
            case R.id.bt3:
                editText.setText(editText.getText().toString().trim()+"3");
                break;
            case R.id.bt4:
                editText.setText(editText.getText().toString().trim()+"4");
                break;
            case R.id.bt5:
                editText.setText(editText.getText().toString().trim()+"5");
                break;
            case R.id.bt6:
                editText.setText(editText.getText().toString().trim()+"6");
                break;
            case R.id.bt7:
                editText.setText(editText.getText().toString().trim()+"7");
                break;
            case R.id.bt8:
                editText.setText(editText.getText().toString().trim()+"8");
                break;
            case R.id.bt9:
                editText.setText(editText.getText().toString().trim()+"9");
                break;


            case R.id.btTambah:
                operasi = "tambah";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.btKurang:
                operasi = "kurang";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.btKali:
                operasi = "kali";
                nilai = Double.parseDouble(editText.getText().toString());
                editText.setText("");
                break;
            case R.id.btBagi:
                operasi = "bagi";
                editText.setText("");
                nilai = Double.parseDouble(editText.getText().toString());
                break;


            case R.id.btEnter:
                if(operasi.equals("tambah")){
                    hasil = nilai + Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("kurang")){
                    hasil = nilai - Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("bagi")){
                    hasil = nilai / Double.parseDouble(editText.getText().toString().trim());
                }
                if(operasi.equals("kali")){
                    hasil = nilai * Double.parseDouble(editText.getText().toString().trim());
                }

                int nilaiTemp = (int) hasil;

                if(nilaiTemp == hasil){
                    editText.setText(String.valueOf((int) hasil));
                }else{
                    editText.setText(String.valueOf(hasil));
                }
                break;
            case R.id.btClear:
                nilai = 0;
                editText.setText("");
                break;
        }
    }
}